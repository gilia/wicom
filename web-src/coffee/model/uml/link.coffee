# link.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A Link between two classes.
#
# @namespace model.uml
class Link extends model.MyModel
    # @param classes {Array<Class>} An array of Class objects,
    #   the first class is the "from" and the second is the "to" class
    #   in a two-linked relation.
    constructor: (@classes, name=null) ->
        if name?
            super(name)
        else
            super(Link.get_new_name())
        @mult = []
        @roles = [[null,null],[null,null]]

    # Set the multiplicity.
    #
    # For example:
    # `[null, null]` or `["0..*", "0..*"]` means from 0..* to 0..*.
    # `[1..*, null]` means from 1..* to 0..*.
    #
    # @param [array] mult An array that describes the multiplicity in strings.
    set_mult : (mult) ->
        i=0
        length=@classes.length
        while i< length
          if (mult[i] == "0..*") or (mult[i] == "0..n") or (mult[i] == "") or (mult[i]==undefined)
              @mult[i] = null
          else
              @mult[i] = mult[i]
          i=i+1

    # Change the from and to roles.
    #
    # @param [[array][array]] roles An array of arrays with two strings, the from and to roles. First array contains user defined roles
    #Second array constains default roles
    set_roles: (@roles) ->
      if (@roles[0][0] == "") or (@roles[0][0] == undefined)
          @roles[0][0]=null
          if @roles[1][0] != null
             @roles[1][0]=@roles[1][0].toLowerCase()
      else
          @roles[0][0]=@roles[0][0].toLowerCase()
      if  (@roles[0][1] == "") or (@roles[0][1] == undefined)
          @roles[0][1]=null
          if @roles[1][1] != null
             @roles[1][1]=@roles[1][1].toLowerCase()
      else
          @roles[0][1]=@roles[0][1].toLowerCase()




    change_mult_to_null : (mult, index) ->
        if (mult == "0..*") or (mult == "0..n")  or  (mult == "") or  (mult == undefined)
            @mult[index] = null

    change_roles_to_null : (roles, index) ->
          if (roles == "0..*") or (roles == "0..n")  or  (roles == "") or  (roles == undefined)
              @roles[index] = null
    #
    # @param class_from an instance of Class.
    set_from : (class_from) ->
        @classes[0] = class_from

    get_from : () ->
        return @classes[0]

    set_to : (class_to) ->
        @classes[1] = class_to

    get_to: () ->
        return @classes[1]

    get_classes: () ->
        return @classes

    # True if a two-linked relation. False otherwise.
    is_two_linked: () ->
        return @classes.length == 2

    # *Implement in the subclass if necesary.*
    #
    # @param parentclass {Class} The Class instance to check.
    # @return `true` if this is a generalization class and has the given parent instance. `false` otherwise.
    has_parent: (parentclass) ->
        return false

    # Is this link associated to the given class?
    #
    # @param c {Class instance} The class to test with.
    #
    # @return {Boolean}
    is_associated: (c) ->
        this.has_parent(c) or @classes.includes(c)

    to_json: () ->
        json = super()
        json.classes = $.map(@classes, (myclass) ->
            myclass.get_name()
        )
        json.multiplicity = @mult
        roles = []
        if @roles[0][0] == null
          roles.push(@roles[1][0])
        else
          roles.push(@roles[0][0])

        if @roles[0][1] == null
          roles.push(@roles[1][1])
        else
          roles.push(@roles[0][1])
        json.roles = roles
        json.type = "association"

        return json

    #
    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if @joint == null
            @joint = []
            if csstheme != null
                @joint.push(factory.create_binary_association(
                    @classes[0].get_classid(),
                    @classes[1].get_classid(),
                    @name,
                    csstheme.css_links,
                    @mult,
                    @roles))
            else
                @joint.push(factory.create_binary_association(
                    @classes[0].get_classid(),
                    @classes[1].get_classid(),
                    @name
                    null,
                    @mult,
                    @roles))

    hasSourceAndTarget: (name,class_a,class_b) ->
      return @classes[0]==class_a and @classes[1]==class_b and @name==name

    # Compare the relevant elements of these links.
    #
    # @param other {Link}
    # @return {boolean}
    same_elts: (other) ->
        if !super(other)
            return false
        if @classes.length != other.classes.length
            return false

        # it must have the same order!
        all_same = true
        for index in [0...@classes.length]
            do (index) =>
                all_same = all_same && @classes[index].same_elts(other.classes[index])

        all_same = all_same && v == other.mult[k] for v,k in @mult

        all_same = all_same && v == other.roles[k] for v,k in @roles


        return all_same


Link.get_new_name = () ->
    if Link.name_number == undefined
        Link.name_number = 0
    Link.name_number = Link.name_number + 1
    return "r" + Link.name_number


exports.model.uml.Link = Link
