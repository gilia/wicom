exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A link from associationIcon to association class.
#
# @namespace model.uml
class LinkWithClass extends model.uml.Link

    # @param classes {Class} Classes involved in this association.
    constructor: (classes, name = null) ->
        super(classes, name)

    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if csstheme == null
            csstheme =
                css_links: null
        if @joint == null
            @joint = []
            @joint.push(factory.create_linkwithclass(
                @classes[0].get_relid(),
                @classes[1].get_classid(),
                @name))

    to_json: () ->

exports.model.uml.LinkWithClass = LinkWithClass
