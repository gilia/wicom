# generalization.coffee --
# Copyright (C) 2017 Giménez, Christian, Angela Oyarzun

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A link to associationIcon.
#
# @namespace model.uml
class LinkToAssociation extends model.uml.Link

    # @param classes {Class} Classes involved in this association.
    constructor: (classes, name = null,@mult,@roles) ->
        super(classes, name)
        this.set_mult(@mult)
        this.set_roles(@roles)


    # Set the multiplicity.
    # @param String mult A String that describes the multiplicity. The mult from the class associated with associationIcon
    set_mult : (@mult) ->
        if (@mult == "0..*") or (@mult == "0..n")  or  (@mult == "")
          @mult=null

    # Set the role.
    #
    # @param [String,String] roles. The role from the class associated with associationIcon. First string is the user defined role and second is default
    set_roles: (@roles) ->
        if (@roles[0] == "") or (@roles[0] == undefined)
          @roles[0]=null

    change_to_null : (mult, index) ->
        if (mult == "0..*") or (mult == "0..n")
            mult[index] = null

    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if csstheme == null
            csstheme =
                css_links: null
        if @joint == null
            @joint = []
            @joint.push(factory.create_to_association_link(
                @classes[0].get_classid(),
                @classes[1].get_relid(),
                @name,
                csstheme.css_links,
                @mult,
                @roles))

    to_json: () ->

exports.model.uml.LinkToAssociation = LinkToAssociation
