# linkisatoentity.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.eer = exports.model.eer ? {}


# @namespace model.eer
class LinkRelToEntity extends model.eer.LinkAttrToEntity
###
    constructor: (@classes, name=null) ->
        super(classes, name)
        @mult = [null]
        @roles = [name]

    get_name: () ->
      return @name

    get_mult: () ->
      return @mult

    get_role: () ->
      return @roles

    # Set the multiplicity.
    #
    # For example:
    # `[null, null]` or `["0..*", "0..*"]` means from 0..* to 0..*.
    # `[1..*, null]` means from 1..* to 0..*.
    #
    # @param [array] mult An array that describes the multiplicity in strings.
    set_mult : (@mult) ->
        this.change_to_null(m) for m in @mult

    # Change the from and to roles.
    #
    # @param [array] roles An array with two strings, the from and to roles.
    set_roles: (@role) ->

    change_to_null : (mult) ->
        if (mult == "0..*") or (mult == "0..n")
            @mult[0] = null

    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if @joint == null
            @joint = []
            if csstheme != null
              @joint.push(factory.create_link_relationship(@classes[0].get_classid(),
                @classes[1].get_relid()
                @name,
                @mult,
                @roles,
                csstheme.css_links))
            else
              @joint.push(factory.create_link_relationship(@classes[0].get_classid(),
                @classes[1].get_relid()
                @name
                @mult,
                @roles,
                null
                ))


    to_json: () ->
        json = super()
        json.type = "relationship"

        return json


LinkRelToEntity.get_new_name = () ->
    if LinkRelToEntity.name_number == undefined
        LinkRelToEntity.name_number = 0
    LinkRelToEntity.name_number = LinkRelToEntity.name_number + 1
    return "r" + LinkRelToEntity.name_number
###
    # @param classes {Class} Classes involved in this relation.

# @namespace model.eer
class LinkRelToEntity extends model.eer.LinkAttrToEntity
    constructor: (classes, name = null,@mult,@roles) ->
        super(classes, name)
        this.set_mult(@mult)
        this.set_roles(@roles)

    # Set the multiplicity.
    # @param String mult A String that describes the multiplicity. The mult from the class associated with relationIcon
    set_mult : (@mult) ->
        if (@mult == "0..*") or (@mult == "0..n") or  (@mult == "")
          @mult=null


    # Set the role.
    #
    # @param [String,String] roles. The role from the class associated with relationIcon. First string is the user defined role and second is default
    set_roles: (@roles) ->
      if (@roles[0] == "") or (@roles[0] == undefined)
        @roles[0]=null

    change_to_null : (mult, index) ->
        if (mult == "0..*") or (mult == "0..n")
            mult[index] = null

    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if csstheme == null
            csstheme =
                css_links: null
        if @joint == null
            @joint = []
            @joint.push(factory.create_from_relation_link(
                @classes[0].get_relid(),
                @classes[1].get_classid(),
                @name,
                csstheme.css_links,
                @mult,
                @roles))

      to_json: () ->
exports.model.eer.LinkRelToEntity = LinkRelToEntity
