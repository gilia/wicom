
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.generalisation = exports.views.uml.generalisation ? this


GenIconOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_generalisationiconoptions").html() )
        this.$el.html(template({geniconid: @geniconid}))

    events:
        "click a#umldeletegeneralization_button" : "delete_generalization",
        "click a#umladdchild_button" : "edit_generalization",

    delete_generalization: () ->
          console.log('REMOVER GEN')
          gui.gui_instance.hide_options()
          gui.gui_instance.delete_class(@geniconid)

    # Add child to generalization
    edit_generalization: () ->
          console.log('AGREGAR HIJO GEN')
          gui.gui_instance.hide_options()
          guiinst.set_isa_state(@geniconid)

    set_geniconid: (@geniconid) ->
        viewpos = graph.getCell(@geniconid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_geniconid: () ->
        return @geniconid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.uml.generalisation.GenIconOptionsView = GenIconOptionsView
