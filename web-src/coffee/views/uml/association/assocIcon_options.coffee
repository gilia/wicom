exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this

AssocIconOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_assocIcon_options").html() )
        this.$el.html(template({reliconid: @reliconid}))

    events:
        "click a#deleteassoc_button" : "delete_association",
        "click a#edit_button" : "edit_association",
        "click a#addclass_button" : "addclass_association",

    delete_association: () ->
          console.log('REMOVER ASSOC')
          gui.gui_instance.hide_options()
          gui.gui_instance.delete_class(@reliconid)

    # Edit roles and multiplicity of class involved in this association
    edit_association: () ->
          console.log('EDITAR CLASE')
          gui.gui_instance.hide_options()
          gui.gui_instance.set_editassociation_classid([@reliconid,"edit",true])

    # Add class to this association
    addclass_association: () ->
          console.log('AGREGAR CLASE')
          mult = [null,""]
          roles = [null,""]
          gui.gui_instance.set_association_state(@reliconid, mult, roles,[name, false,"nary"])

    set_reliconid: (@reliconid) ->
        viewpos = graph.getCell(@reliconid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_reliconid: () ->
        return @reliconid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.uml.association.AssocIconOptionsView = AssocIconOptionsView
