<div class="naryOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden" id="umlrelationoptions_classid"  name="classid"  value="<%= classid %>" />
    <div data-role="controlgroup" data-mini="true" data-type="horizontal" style="float: left">
	     <form id="name-rel">
	        <input data-mini="true" placeholder="name" type="text" size="4" maxlength="10" id="nary_name" />
	        <div data-role="controlgroup" data-mini="true" data-type="horizontal">
		          <a class="ui-btn ui-corner-all ui-icon-arrow-r ui-btn-icon-notext" type="button" id="nary_button">Association</a>
		          <a class="ui-btn ui-corner-all ui-btn-icon-notext" type="button" id="nary_class_button">Association Class</a>
	        </div>
	     </form>
    </div>
</div>
