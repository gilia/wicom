exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this


EditAssocView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_editassoc").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click a#done_button" : "edit_assoc",

    # Retrieve the destination role and multiplicity
    cardto: () ->
        too_1 = $('#narycardto-1').val()
        too_2 = $('#narycardto-2').val()

        if (too_1 == "") && (too_2 == "")
          too_1 = too_1.concat "0"
          too_aux = too_1.concat ".."
          too_2 = too_2.concat "*"
          @too = too_aux.concat too_2
        else
          if (too_1 == "") && (too_2 != "")
            too_1 = too_1.concat "0"
            too_aux = too_1.concat ".."
            @too = too_aux.concat too_2
          else
            if (too_1 != "") && (too_2 == "")
              too_aux = too_1.concat ".."
              too_2 = too_2.concat "*"
              @too = too_aux.concat too_2
            else
              too_aux = too_1.concat ".."
              @too = too_aux.concat too_2

        @to_role = $('#naryrole-to').val()

    # Edit roles and multiplicity of class involved in this association
    edit_assoc: () ->
        this.cardto()
        mult = []
        mult[0] = null
        mult[1] = @too
        roles = []
        roles[0] = null
        roles[1] = @to_role
        gui.gui_instance.set_association_state(@classid, mult, roles,[name, false,"binary"])

    set_reliconid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_reliconid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.uml.association.EditAssocView =EditAssocView
