exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this


NaryOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_nary_options").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click a#nary_button" : "new_relation",
        "click a#nary_class_button" : "new_assoc_class"

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields.
    #
    # Callback, used when the user clicks on the association button.
    new_relation: () ->
        mult = []
        mult[0] = ""
        mult[1] = ""
        roles = []
        roles[0] = ""
        roles[1] = ""
        name = $("#nary_name").val()
        if name==""
          name=null
        gui.gui_instance.set_association_state(@classid, mult, roles, [name, false,"nary"])

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields. *Use an association class in the middle.*
    #
    # Callback, used when the user clicks on the create association class button.
    new_assoc_class: (from, too) ->
        mult = []
        mult[0] = ""
        mult[1] = ""
        roles = []
        roles[0] = ""
        roles[1] = ""
        name = $("#nary_name").val()
        if name==""
          name=null
        @hide()
        gui.gui_instance.set_association_state(@classid, mult, roles, [name, true,"nary"])


    # Map the Option value to multiplicity string.
    #
    # @param str {string} The value string.
    # @return {String} A string that represent the multiplicity as in UML.
    map_to_mult : (str) ->
        switch str
            when "zeromany" then "0..*"
            when "onemany" then "1..*"
            when "zeroone" then "0..1"
            when "oneone" then "1..1"


    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.uml.association.NaryOptionsView = NaryOptionsView
