exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.association = exports.views.uml.association ? this

AssociationTypeView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_association_type").html() )
        this.$el.html(template({classid: @classid}))

    events:
        'click a#assoc_type_button' : 'new_association'

    new_association: (event) ->
        binary = $("#chk-binary").prop("checked")
        gui.gui_instance.hide_options()
        if binary
          type="binary"
        else
          type="nary"
        gui.gui_instance.set_editassociation_classid([@classid,binary,type])
        @hide()

    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)

exports.views.uml.association.AssociationTypeView = AssociationTypeView
