<div class="assocOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden" id="relationoptions_classid"  name="classid"  value="<%= classid %>" />
    <a class="ui-btn ui-corner-all ui-icon-arrow-u ui-btn-icon-notext" type="button" title="Select Association Type" id="assoc_type_button">Association</a>
    <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">
      <input type="radio" id="chk-binary" name="assoc" value="binary">
      <label for="chk-binary">Binary</label>
      <input type="radio" id="chk-nary" name="assoc" value="nary">
      <label for="chk-nary">N-ary</label>
    </fieldset>
</div>
