<div class="associconOptions" data-role="controlgroup" data-mini="true"
     data-type="horizontal" style="visible:false, z-index:1, position:absolute" >
    <input type="hidden" id="voptions_reliconid" name="reliconid" value="<%= reliconid %>" />
    <a class="ui-btn ui-corner-all ui-icon-edit ui-btn-icon-notext" type="button" title="Edit Association" id="edit_button">Edit Association</a>
    <a class="ui-btn ui-icon-plus ui-btn-icon-notext" type="button" title="Add Class" id="addclass_button">Add Class</a>
    <a class="ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext" type="button" title="Delete Association" id="deleteassoc_button">Delete</a>
</div>
