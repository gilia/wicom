exports = exports ? this
exports.views = exports.views ? this
exports.views.eer = exports.views.eer ? this
exports.views.eer.relationship = exports.views.eer.relationship ? this


BinaryOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_binary_options").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click a#binary_button" : "new_relation",
        "click a#binary_attr_button" : "new_rel_attr"


    cardfrom: () ->
        from_1 = $('#eercardfrom-1').val()
        from_2 = $('#eercardfrom-2').val()

        if (from_1 == "") && (from_2 == "")
          from_1 = from_1.concat "0"
          from_aux = from_1.concat ".."
          from_2 = from_2.concat "*"
          @from = from_aux.concat from_2
        else
          if (from_1 == "") && (from_2 != "")
            from_1 = from_1.concat "0"
            from_aux = from_1.concat ".."
            @from = from_aux.concat from_2
          else
            if (from_1 != "") && (from_2 == "")
              from_aux = from_1.concat ".."
              from_2 = from_2.concat "*"
              @from = from_aux.concat from_2
            else
              from_aux = from_1.concat ".."
              @from = from_aux.concat from_2

        @from_role = $('#eerrole-from').val()

    # Retrieve the destination role and multiplicity
    cardto: () ->
        too_1 = $('#eercardto-1').val()
        too_2 = $('#eercardto-2').val()

        if (too_1 == "") && (too_2 == "")
          too_1 = too_1.concat "0"
          too_aux = too_1.concat ".."
          too_2 = too_2.concat "*"
          @too = too_aux.concat too_2
        else
          if (too_1 == "") && (too_2 != "")
            too_1 = too_1.concat "0"
            too_aux = too_1.concat ".."
            @too = too_aux.concat too_2
          else
            if (too_1 != "") && (too_2 == "")
              too_aux = too_1.concat ".."
              too_2 = too_2.concat "*"
              @too = too_aux.concat too_2
            else
              too_aux = too_1.concat ".."
              @too = too_aux.concat too_2

        @to_role = $('#eerrole-to').val()

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields.
    #
    # Callback, used when the user clicks on the association button.
    new_relation: () ->
        this.cardfrom()
        this.cardto()

        mult = []
        mult[0] = @from
        mult[1] = @too
        roles = []
        roles[0] = @from_role
        roles[1] = @to_role
        name = $("#binary_name").val()
        if name==""
          name=null
        gui.gui_instance.set_association_state(@classid, mult, roles, [name, false,"binary"])

    # Create a new relation with the information from the role, multiplicity and
    # association name input fields. *Use an association class in the middle.*
    #
    # Callback, used when the user clicks on the create association class button.
    new_rel_attr: (from, too) ->
        this.cardfrom()
        this.cardto()

        mult = []
        mult[0] = @from
        mult[1] = @too
        roles = []
        roles[0] = @from_role
        roles[1] = @to_role
        name = $("#binary_name").val()
        if name==""
          name=null
        @hide()
        gui.gui_instance.set_association_state(@classid, mult, roles, [name, true,"binary"])


    # Map the Option value to multiplicity string.
    #
    # @param str {string} The value string.
    # @return {String} A string that represent the multiplicity as in eer.
    map_to_mult : (str) ->
        switch str
            when "zeromany" then "0..*"
            when "onemany" then "1..*"
            when "zeroone" then "0..1"
            when "oneone" then "1..1"


    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)




exports.views.eer.relationship.BinaryOptionsView = BinaryOptionsView
