exports = exports ? this
exports.views = exports.views ? this
exports.views.eer = exports.views.eer ? this
exports.views.eer.relationship = exports.views.eer.relationship ? this

RelIconOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_relIcon_options").html() )
        this.$el.html(template({reliconid: @reliconid}))

    events:
        "click a#deleterel_button" : "delete_relation",
        "click a#edit_button" : "edit_relation",
        "click a#addclass_button" : "addclass_relation",
        "click a#addattr_button" : "addattr_relation",



    delete_relation: () ->
          console.log('REMOVER REL')
          gui.gui_instance.hide_options()
          gui.gui_instance.delete_class(@reliconid)

    # Edit roles and multiplicity of class involved in this relationship
    edit_relation: () ->
          console.log('EDITAR CLASE')
          gui.gui_instance.hide_options()
          gui.gui_instance.set_editassociation_classid(@reliconid)

    # Add class to this relationship
    addclass_relation: () ->
          console.log('AGREGAR CLASE')
          mult = [null,""]
          roles = [null,""]
          gui.gui_instance.set_association_state(@reliconid, mult, roles,[name,"binary"])

    # Add attribute to this relationship
    addattr_relation: () ->
          console.log('AGREGAR ATRIBUTO')
          mult = [null,""]
          roles = [null,""]
          gui.gui_instance.set_association_state(@reliconid, mult, roles,[name,"linkreltoattr"])

    set_reliconid: (@reliconid) ->
        viewpos = graph.getCell(@reliconid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_reliconid: () ->
        return @reliconid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)


exports.views.eer.relationship.RelIconOptionsView = RelIconOptionsView
