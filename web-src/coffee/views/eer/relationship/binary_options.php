<div class="binaryOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden" id="eerrelationoptions_classid"  name="classid"  value="<%= classid %>" />
    <div data-role="controlgroup" data-mini="true" data-type="horizontal" style="float: left">
      <form id="left-rel">
         <input type="hidden" id="eerrelationoptions_classid" name="classid" value="<%= classid %>" />
         <input data-mini="true" placeholder="0" type="text" id="eercardfrom-1" size="2" maxlength="4" />
         <input type="hidden" id="eerrelationoptions_classid" name="classid" value="<%= classid %>" />
         <input data-mini="true" placeholder="*" type="text" id="eercardfrom-2" size="2" maxlength="4" />
         <input data-mini="true" placeholder="role1" type="text" id="eerrole-from" size="2" maxlength="4" />
     </form>
    </div>

    <div data-role="controlgroup" data-mini="true" data-type="horizontal" style="float: left">
      <form id="name-rel">
         <input data-mini="true" placeholder="name" type="text" size="4" maxlength="10" id="binary_name" />
         <div data-role="controlgroup" data-mini="true" data-type="horizontal">
             <a class="ui-btn ui-corner-all ui-icon-arrow-r ui-btn-icon-notext" type="button" id="binary_button">Relation</a>
             <a class="ui-btn ui-corner-all ui-btn-icon-notext" type="button" id="binary_attr_button">Relation Attribute</a>
         </div>
      </form>
    </div>

    <input type="hidden" id="eerrelationoptions_classid"  name="classid"  value="<%= classid %>" />
    <div data-role="controlgroup" data-mini="true" data-type="horizontal" style="float: right">
      <form id="right-rel">
         <input type="hidden" id="eerrelationoptions_classid" name="classid" value="<%= classid %>" />
         <input data-mini="true" placeholder="0" type="text" id="eercardto-1" size="2" maxlength="4" />
         <input type="hidden" id="eerrelationoptions_classid" name="classid" value="<%= classid %>" />
         <input data-mini="true" placeholder="*" type="text" id="eercardto-2" size="2" maxlength="4" />
         <input data-mini="true" placeholder="role2" type="text" id="eerrole-to" size="2" maxlength="4" />
      </form>
    </div>
</div>
