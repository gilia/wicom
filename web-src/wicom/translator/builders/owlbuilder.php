<?php
/*

   Copyright 2017 GILIA

   Author: Giménez, Christian and Braun, Germán

   owlbuilder.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Translator\Builders;

use function \load;
load("documentbuilder.php");
load("owldocument.php", "../documents/");

use Wicom\Translator\Documents\OWLDocument;

class OWLBuilder extends DocumentBuilder{

    protected $actual_kb = null;

    function __construct(){
        $this->product = new OWLDocument;
        $this->min_max = [];
    }

    /**
    @param $ontologyIRI An Array containing the IRI for the ontology: ["prefix" => "", "iri" => ""]
    @param $IRIs An Array containing the OWL 2 header IRIs: [["prefix" => "", "iri" => ""], ... , ["prefix" => "", "iri" => ""]]
    @param $PREFIXES An Array containing the IRIs and Prefixes: [["prefix" => "", "iri" => ""], ... , ["prefix" => "", "iri" => ""]]
    */
    public function insert_header_owl2($ontologyIRI = null, $IRIs = [], $PREFIXES = []){
        $this->product->start_document($ontologyIRI, $IRIs);

//        if (empty($ontologyIRI)){
        if ($ontologyIRI == null){
          $this->actual_kb = "http://crowd.fi.uncoma.edu.ar/kb1/";
        } else {
          $this->actual_kb = $ontologyIRI;//["iri"];
        }

      $this->product->set_ontology_prefixes($PREFIXES);
    }

    public function insert_class($name, $col_attrs = []){
        $this->product->insert_class($name);
    }

    public function insert_dataproperty($name, $col_attrs = []){
        $this->product->insert_dataproperty($name);
    }

    public function insert_subclassof($child, $father){
        $this->product->insert_subclassof($child, $father);
    }

    public function insert_footer(){
        $this->product->end_document();
    }

    /**
       @todo Move this into the Strategy.
    */
    public function insert_class_min($classname, $minname){
        if (key_exists($classname, $this->min_max)){
            $this->min_max[$classname][0] = $minname;
        }else{
            $this->min_max[$classname] = [$minname, null];
        }
        $this->product->insert_class($minname);
    }

    /**
       @todo Move this into the Strategy.
     */
    public function insert_class_max($classname, $maxname){
        if (key_exists($classname, $this->min_max)){
            $this->min_max[$classname][1] = $maxname;
        }else{
            $this->min_max[$classname] = [null, $maxname];
        }

        $this->product->insert_class($maxname);
    }



    /**
       Reimplementation because we have to finish the product
       before getting it.
     */
    public function get_product($finish=false){
        if ($finish){
            $this->product->end_document();
        }
        return $this->product;
    }

    public function insert_owl2($text){
        $this->product->insert_owl2($text);
    }

    /**
       @name DL list translation
    */
    ///@{
    ///@}
    // DL List Translation
}
?>
