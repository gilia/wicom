<?php
/*

   Copyright 2016 Giménez, Christian. Germán Braun.

   Author: Giménez, Christian

   wicomtest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

// use function \load;
load("wicom.php", "common/");
load("uml.php", "common/");
load("config.php", "config/");

use Wicom\Wicom;
use Wicom\UML_Wicom;

class WicomTest extends PHPUnit\Framework\TestCase
{
    public function test_is_satisfiable_UML(){
        $input = '{"classes": [{"attrs":[], "methods":[], "name": "Hi World"}]}';
        $expected = <<<EOT
       {
           "satisfiable": {
               "kb" : true,
               "classes" : ["Hi World"]
           },
           "unsatisfiable": {
              	"classes" : []
           },
           "suggestions" : {
              	"links" : []
           },
           "reasoner" : {
              	"input" : "",
              	"output" : ""
           }
       }
EOT;


        $wicom = new Wicom();
        // $GLOBALS['config']['temporal_path'] = "../../temp/";
        $answer = $wicom->is_satisfiable($input);

        /*print("\n\$answer = ");
        print_r($answer);
        print("\n");*/

        $answer->set_reasoner_input("");
        $answer->set_reasoner_output("");
        $actual = $answer->to_json();

        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
    }


    public function test_full_reasoning_UML(){
        $input = '{"classes":[{"name":"Person","attrs":[
                                                {"name":"dni","datatype":"String"},
                                                {"name":"firstname","datatype":"String"},
                                                {"name":"surname","datatype":"String"},
                                                {"name":"birthdate","datatype":"Date"}],
                                                "methods":[],"position":{"x":287,"y":38}},
                              {"name":"Student","attrs":[
                                                {"name":"id","datatype":"String"},
                                                {"name":"enrolldate","datatype":"Date"}],
                                                "methods":[],"position":{"x":538,"y":251}}],
                    "links":[{"name":"r1","classes":["Student"],
                                          "multiplicity":null,
                                          "roles":[null,null],
                                          "type":"generalization",
                                          "parent":"Person",
                                          "constraint":[]}]
                  }';
        $expected = <<<EOT
       {
           "satisfiable": {
               "kb" : true,
               "classes" : ["Person", "Student"]
           },
           "unsatisfiable": {
                "classes" : []
           },
           "suggestions" : {
                "links" : []
           },
           "reasoner" : {
                "input" : "",
                "output" : ""
           }
       }
EOT;


        $wicom = new UML_Wicom();
        // $GLOBALS['config']['temporal_path'] = "../../temp/";
        $answer = $wicom->full_reasoning($input);

        /*print("\n\$answer = ");
        print_r($answer);
        print("\n");*/

        $answer->set_reasoner_input("");
        $answer->set_reasoner_output("");
        $actual = $answer->to_json();

        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
    }


    public function test_full_reasoning_UML_Inconsistent(){
        $input = '{"classes":[{"name":"Person","attrs":[
                                                {"name":"dni","datatype":"String"},
                                                {"name":"firstname","datatype":"String"},
                                                {"name":"surname","datatype":"String"},
                                                {"name":"birthdate","datatype":"Date"}],
                                                "methods":[],"position":{"x":287,"y":38}},
                              {"name":"Student","attrs":[
                                                {"name":"id","datatype":"String"},
                                                {"name":"enrolldate","datatype":"Date"}],
                                                "methods":[],"position":{"x":538,"y":251}},
                              {"name":"NoStudent",
                                                "attrs":[],
                                                "methods":[],
                                                "position":{"x":164,"y":274}}],
                    "links":[{"name":"r1","classes":["NoStudent","Student"],
                                  "multiplicity":null,
                                  "roles":[null,null],
                                  "type":"generalization","parent":"Person",
                                  "constraint":["disjoint","covering"]},
                              {"name":"r2","classes":["Student"],
                                  "multiplicity":null,
                                  "roles":[null,null],
                                  "type":"generalization","parent":"NoStudent",
                                  "constraint":[]}]
                  }';
        $expected = <<<EOT
       {
           "satisfiable": {
               "kb" : true,
               "classes" : ["Person", "NoStudent"]
           },
           "unsatisfiable": {
                "classes" : ["Student"]
           },
           "suggestions" : {
                "links" : []
           },
           "reasoner" : {
                "input" : "",
                "output" : ""
           }
       }
EOT;


        $wicom = new UML_Wicom();
        // $GLOBALS['config']['temporal_path'] = "../../temp/";
        $answer = $wicom->full_reasoning($input);

        /*print("\n\$answer = ");
        print_r($answer);
        print("\n");*/

        $answer->set_reasoner_input("");
        $answer->set_reasoner_output("");
        $actual = $answer->to_json();

        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
    }
}
